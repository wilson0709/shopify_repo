#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
use amazon rds to create a mysql instance
design the table to recieve the daily currency rates
- database: shopify
- table: currency_rates
  - DATE DATE
  - CURRENCY VARCHAR(3)
  - RATE NUMBER
- port: 3306
create mysql function SHOPIFY_CURRENCY_CONVERT()
- e.g. SELECT SHOPIFY_CURRENCY_CONVERT(1, ‘USD’, ‘HKD’, ‘2021-07-24’)
update_shopify_currency_rates.py is hosted on Bitbucket
design a reliable workflow that allow updating of the table on a daily basis
"""

'''
DELIMITER $$

CREATE FUNCTION SHOPIFY_CURRENCY_CONVERT(
	   AMOUNT INT,
	   FROM_CUR   VARCHAR(3),
	   TO_CUR     VARCHAR(3),
	   DATE       DATE
) 
RETURNS DECIMAL(25,15)
DETERMINISTIC
BEGIN
	DECLARE FROM_RATE DECIMAL(30,20);
	DECLARE TO_RATE DECIMAL(30,20);
	
	SELECT A.RATE 
	INTO FROM_RATE 
	FROM shopify.currency_rates A
	WHERE A.CURRENCY=FROM_CUR AND A.RATE_DATE=DATE;
	
	SELECT A.RATE 
	INTO TO_RATE 
	FROM shopify.currency_rates A
	WHERE A.CURRENCY=TO_CUR AND A.RATE_DATE=DATE;
	
	RETURN AMOUNT * FROM_RATE / TO_RATE;
END
$$

DELIMITER ;
'''

'''
SELECT SHOPIFY_CURRENCY_CONVERT(1, 'USD', 'HKD', '2021-08-21');
'''

import requests, json, re
from datetime import date
import pymysql
import pandas as pd
import argparse


parser = argparse.ArgumentParser(description="""
update daily currency rate
""")
parser.add_argument("--url")
parser.add_argument("--mysql_host")
parser.add_argument("--mysql_port")
parser.add_argument("--mysql_user")
parser.add_argument("--mysql_password")
parser.add_argument("--mysql_database")
parser.add_argument("--mysql_table")

args = parser.parse_args()

url = args.url
host = args.mysql_host
port = args.mysql_port
user = args.mysql_user
password = args.mysql_password
database = args.mysql_database
table = args.mysql_table

today = date.today().strftime('%Y-%m-%d')

html_data=requests.get(url).text
json_data=re.search(r'rates: (\{.*?\}),', html_data).group(1)
data=json.loads(json_data)
rate_list = [(today,)+x for x in [(k, v) for k, v in data.items()]]


def mysql_connect():
	return pymysql.connect(host, user, password, database)
	print('created connection')

def mysql_ddl(sql, connect):
	cnxn = connect
	cursor = cnxn.cursor()
	cursor.execute(sql)
	cnxn.commit()
	cursor.close()
	cnxn.close()
	del cnxn, cursor

def mysql_cur_executemany(list_of_tuples, ddl, connect):
	cnxn = connect
	cursor = cnxn.cursor()
	cursor.executemany(ddl, list_of_tuples)
	cnxn.commit()
	cursor.close()
	cnxn.close()
	del cnxn, cursor

def mysql_append_df(df, table, connect):
	rows=[tuple(x) for x in df.values]
	join_str = ','.join(['%s' for x in range(df.shape[1])])
	insert = """INSERT INTO """ + table + """ VALUES (""" + join_str + """);"""
	mysql_cur_executemany(rows, insert, connect)


create_table_ddl = """
	CREATE TABLE IF NOT EXISTS shopify.currency_rates  (
	RATE_DATE DATE,
	CURRENCY VARCHAR(3),
	RATE DECIMAL(30,20)
	)
"""
mysql_ddl(create_table_ddl, mysql_connect())
# create table if not exists
mysql_append_df(pd.DataFrame(rate_list), table, mysql_connect())
# update currency rate
print('updated below data')
print(rate_list)


















