python update_shopify_currency_rates.py \
	--url https://cdn.shopify.com/s/javascripts/currencies.js \
	--mysql_host 127.0.0.1 \
	--mysql_port 3306 \
	--mysql_user root \
	--mysql_password password \
	--mysql_database shopify \
	--mysql_table currency_rates
